import React from 'react';
//Pull in AppNavigator from the navigation folder
import AppNavigator from './src/navigations/AppNavigator'
import {Provider} from 'react-redux';
import store from './src/store';

export default function App() {
  return (
    <Provider store={store}>
    <AppNavigator/> 
    </Provider>
  );
}