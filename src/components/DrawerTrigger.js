import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';

// withNavigation allows components to dispatch navigation actions
import { withNavigation } from 'react-navigation';

// DrawerActions is a specific type of navigation dispatcher
import { DrawerActions } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class DrawerTrigger extends React.Component {
    render() {
        return (
            <TouchableOpacity style={styles.trigger}
                onPress={() => {
                    this.props.navigation.dispatch(DrawerActions.openDrawer())
                }}
            >
                <Icon type="EvilIcons" name="navicon"
                    size={25}
                    color='black' />
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    trigger: {
        marginLeft: 15.5,
        width: 60,
        height: 20,
        backgroundColor: '#ffffff'
    }
});

export default withNavigation(DrawerTrigger);