import api from "./../apis";
import Urls from "./../apis/urls";
import { Alert } from 'react-native';

export const POST_REGISTER = 'post_register';
export const POST_REGISTER_SUCCESS = 'post_register_success';
export const POST_REGISTER_FAIL = 'post_register_fail';

export const GET_PARTICIPANTS_LIST = 'get_participants';
export const GET_PARTICIPANTS_SUCCESS = 'get_participants_success';
export const GET_PARTICIPANTS_FAIL = 'get_participants_fail';

export const loadParticipantsData = () => {
    return async (dispatch) => {
        dispatch({ type: GET_PARTICIPANTS_LIST });

        const { statusCode, message, errorMessage, data } = await api({
            method: 'GET',
            url: Urls.GET_PARTICIPANT_DATA,
        });

        if (statusCode === 200) {
            dispatch({ type: GET_PARTICIPANTS_SUCCESS, payload: data });
            
        } else {
            dispatch({
                type: GET_PARTICIPANTS_FAIL,
                payload: { error: errorMessage }
            })
        }
    };
};

export const postRegister = () => {
    return async (dispatch) => {
        dispatch({ type: POST_REGISTER });

        const { statusCode, message, errorMessage, data } = await api({
            method: 'POST',
            url: Urls.REGISTER,
        });

        if (statusCode === 200) {
            dispatch({ type: POST_REGISTER_SUCCESS, payload: data });
            if(data!== null){
                alert(data.status)
            }
        } else {
            dispatch({
                type: POST_REGISTER_FAIL,
                payload: { error: errorMessage }
            })
        }
    };
};
