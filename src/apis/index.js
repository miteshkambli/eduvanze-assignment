import axios from 'axios';


/**
 * Create an Axios Client with defaults
 */
const client = axios.create({
     baseURL: 'https://6a6ce012-a19a-4c6a-8a1e-41b978133482.mock.pstmn.io' // Live

});


/**
 * Request Wrapper with default success/error actions
 */
const api = function (options) {
    const onSuccess = function (response) {
        console.log('Request Successful!', response);

        let data = response.data;
        if (data.data === undefined) {
            if (data.payload)
                data['data'] = data.payload;
            else
                data['data'] = JSON.parse(JSON.stringify(data));
        }

        if (!data.statusCode)
            data['statusCode'] = response.status;
        return data;
    };

    const onError = function (error) {
        console.log('Request Failed:', error.config);
        let message = null;

        if (error.response) {
            // Request was made but server responded with something
            // other than 2xx
            console.log('Status:', error.response.status);
            console.log('Data:', error.response.data);
            console.log('Headers:', error.response.headers);
            message = error.response.message;

        } else {
            // Something else happened while setting up the request
            // triggered the error
            console.log('Error Message:', error.message);
            message = error.message;
        }
        if (!message)
            // message = 'Network error';
            message = 'Something went wrong';

        // Toaster.show(message);

        return {
            statusCode: 500,
            errorMessage: message
        };
    };

    console.log(options.url, options.data);

    return client(options)
        .then(onSuccess)
        .catch(onError);
};

export default api;
