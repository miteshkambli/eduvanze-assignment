import Colors from "./colorsStyles";

let TableStyles;
export default TableStyles = {
    row: {flex: 1, alignSelf: 'stretch', flexDirection: 'row'},
    cell: {flex: 1, alignSelf: 'stretch'},
    oddCell: {flex: 1, alignSelf: 'stretch'},
    evenCell: {flex: 1, alignSelf: 'stretch', backgroundColor: Colors.gray_light},
}
