import React from 'react';
import { View, Text } from 'react-native';
import { createDrawerNavigator } from 'react-navigation-drawer';

import Registration from '../screens/Registration/Registration';
import ParticipantsList from '../screens/ParticipantsList/ParticipantsList';
import ChartContainer from '../screens/Chart/index';

const DrawerNavigator = createDrawerNavigator({
    Register: Registration,
    Participants: ParticipantsList,
    Chart: ChartContainer,

});

export default DrawerNavigator;