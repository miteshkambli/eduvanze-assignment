import {combineReducers} from 'redux';
import DummyReducer from './DummyReducer';
import ListParticipantReducers from './ParticipantsReducers/ListParticipantReducers';
import RegisterReducers from './RegisterReducers/RegisterReducers';


export default combineReducers({
  dummy: DummyReducer,
  listParticipant: ListParticipantReducers,
  Register: RegisterReducers  
  

});
