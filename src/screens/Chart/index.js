import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
} from "react-native-chart-kit";
import React from 'react'
import { ScrollView, Dimensions, Text, View } from 'react-native'
import { data } from './data'
import Header from '../../components/Header.js'


export default class ChartContainer extends React.Component {

    render() {
        const chartConfig = {
            backgroundGradientFrom: "#1E2923",
            backgroundGradientFromOpacity: 0,
            backgroundGradientTo: "#08130D",
            backgroundGradientToOpacity: 0.5,
            color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            strokeWidth: 2, // optional, default 3
            barPercentage: 0.5,
            useShadowColorFromDataset: false // optional
        };
        const screenWidth = Dimensions.get("window").width;

        return (
            <React.Fragment>
                <Header />
                <ScrollView>
                    <Text style={{ margin: 10 }}>Number of people by localities</Text>
                    <View>
                        <LineChart
                            data={{
                                labels: ["Mumbai", "Navi Mumbai", "Thane", "Pune"],
                                datasets: [
                                    {
                                        data: [
                                            Math.random() * 100,
                                            Math.random() * 100,
                                            Math.random() * 100,
                                            Math.random() * 100,
                                        ]
                                    }
                                ]
                            }}
                            width={Dimensions.get("window").width} // from react-native
                            height={220}
                            yAxisLabel="$"
                            yAxisSuffix="k"
                            yAxisInterval={1} // optional, defaults to 1
                            chartConfig={{
                                backgroundColor: "#e26a00",
                                backgroundGradientFrom: "#fb8c00",
                                backgroundGradientTo: "#ffa726",
                                decimalPlaces: 2, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                style: {
                                    borderRadius: 16
                                },
                                propsForDots: {
                                    r: "6",
                                    strokeWidth: "2",
                                    stroke: "#ffa726"
                                }
                            }}
                            bezier
                            style={{
                                marginVertical: 8,
                                borderRadius: 16
                            }}
                        />
                    </View>
                    <Text style={{ margin: 10 }}>Number of people in a given age range (13-18, 18-25 and 25+).</Text>

                    <ProgressChart
                        data={data}
                        width={screenWidth}
                        height={220}
                        strokeWidth={16}
                        radius={32}
                        chartConfig={chartConfig}
                        hideLegend={false}
                    />
                    <Text style={{ margin: 10 }}>Professionals and students count.</Text>

                    <BarChart
                        data={{
                            labels: [
                                'Professional',
                                'Student',
                            ],
                            datasets: [
                                {
                                    data: [30, 45],
                                },
                            ],
                        }}
                        width={Dimensions.get('window').width - 16}
                        height={220}
                        chartConfig={{
                            backgroundColor: '#1cc910',
                            backgroundGradientFrom: '#eff3ff',
                            backgroundGradientTo: '#efefef',
                            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                            style: {
                                borderRadius: 16,
                            },
                        }}
                        style={{
                            marginVertical: 8,
                            borderRadius: 16,
                        }}
                    />
                </ScrollView>
            </React.Fragment>
        )
    }
}