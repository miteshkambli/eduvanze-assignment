// Mock data object used for LineChart and BarChart

const data = {
  labels: ["Age 13-18", "Age 18-25", "Age 25+"], // optional
  data: [0.4, 0.6, 0.8]
};
  // Mock data object used for Contribution Graph

  const barchart = {
    labels: ["January", "February", "March", "April", "May", "June"],
    datasets: [
      {
        data: [20, 45, 28, 80, 99, 43]
      }
    ]
  };



  export { data }