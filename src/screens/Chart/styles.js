export default {
    labelStyle: {
        color: '#fffff',
        marginVertical: 10,
        textAlign: 'center',
        fontSize: 16
    },
    graphStyle: {
        marginVertical: 8,
    }
}