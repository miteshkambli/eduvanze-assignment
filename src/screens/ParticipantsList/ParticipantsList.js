import React from 'react';
import {
  View, Text, ScrollView,
} from 'react-native';
import styles from './styles';
import {
  Body,
  Icon,
  ListItem,
  Right,
} from 'native-base';
import Header from '../../components/Header.js'
import Colors from "../../styles/colorsStyles";
import { loadParticipantsData } from '../../actions';
import { connect } from 'react-redux';

class ParticipantsList extends React.Component {

  static navigationOptions = {

  };

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    this.props.loadParticipantsData();
  }
  renderItem(data) {
    if (data !== null) {
      return data.map((item, index) => {

        return (
          <ListItem>
            <Body style={{ marginBottom: 5 }}>
              <Text style={styles.name}>{item.user_name.toLocaleUpperCase()}</Text>
              <Text note style={styles.secondaryTitle}>
                Age : {item.age}
              </Text>
              <Text note style={styles.secondaryTitle}>
                Locality : {item.locality.toLocaleUpperCase()}
              </Text>
            </Body>

            <Right>
              <View style={styles.rightAlign}>
                <Icon
                  name="rightcircle"
                  type="AntDesign"
                  style={{ color: Colors.black, fontSize: 16 }}
                />
              </View>
            </Right>
          </ListItem>
        );
      });
    }
    return (
     <View style={{ marginTop : '60%', alignItems: 'center',
     justifyContent: 'center'}}>
       <Text>Loading ...</Text>
     </View>
    )
  }
  render() {
    const { data, loading } = this.props.listParticipant;

    return (
      <React.Fragment>
        <Header />
        <View style={{ flex: 1 }}>
          <View style={{ alignItems: 'center' }}>
            <Text
              style={{
                fontSize: 30,
                fontWeight: 'bold',
                justifyContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                marginTop: 10,
              }}>
              Participants List
          </Text>
          </View>
          <ScrollView style={{ marginBottom: 15 }}>
            {this.renderItem(data)}
          </ScrollView>
        </View>

      </React.Fragment>
    );
  }
}


const mapStateToProps = (state) => ({
  listParticipant: state.listParticipant,
});

export default connect(mapStateToProps, { loadParticipantsData })(
  ParticipantsList,
);
