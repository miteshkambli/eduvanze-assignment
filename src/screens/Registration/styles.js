import color from '../../Constants/colorConstants';
import { Dimensions } from 'react-native'
import { FontStyle, LightFontStyle } from "../../styles/fontsStyles";
import colorsStyles from "./../../Constants/colorConstants";

export default {
    imageBackground: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    performanceSlider: {
        height: 2 * Dimensions.get('window').height / 5
    },
    sliderCard: {
        marginTop: 10,
        backgroundColor: colorsStyles.white,
        width: '90%',
        marginLeft: '5%',
        borderRadius: 10,
        maxHeight: 180
    },
    sliderCardHeading: {
        marginHorizontal: 15,
        marginVertical: 5,
    },
    sliderHeadingSeparator: {
        width: '100%',
        height: 1,
        backgroundColor: color.LineColor
    },
    sliderInternalContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 10
    },
    sliderBar: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '75%',
        alignItems: 'center',
        marginLeft: 10,
        marginBottom: 10
    },
    barContainer: {
        marginTop: 5
    },
    rankText: {
        width: 30,
        fontSize: 11,
        color: colorsStyles.gray
    },
    rankDetailText: {
        fontSize: 11,
        color: colorsStyles.gray
    },
    barBackground: {
        width: 100,
        height: 6,
        borderRadius: 3,
        backgroundColor: color.ProgressBarBackground
    },
    leftScore: { alignItems: 'center', backgroundColor: colorsStyles.dashboard_score + '90', padding: 10, borderWidth: 2, borderRadius: 5, borderColor: colorsStyles.dashboard_score },
    barFill: {
        height: 6,
        borderBottomLeftRadius: 3,
        borderTopLeftRadius: 3,
        backgroundColor: color.ProgressFill
    },
    bottomTextContainer: {
        flexDirection: 'row',
        marginHorizontal: 15,
        marginVertical: 5,
    },
    leftText: {
        color: colorsStyles.gray,
        fontSize: 12,
        marginRight: 95,
        marginLeft: 10,
    },
    rightText: {
        color: colorsStyles.gray,
        fontSize: 12,
    },
    welcomeText: {
        color: color.WhiteColor,
        fontSize: 16,
        marginLeft: 20,
        ...LightFontStyle.fontMedium
    },
    nameText: {
        color: color.WhiteColor,
        fontSize: 19,
        marginLeft: 20,
        ...FontStyle.fontLarge
    },
    posLocText: {
        color: color.WhiteColor,
        fontSize: 17,
        marginLeft: 20,
        ...LightFontStyle.fontMedium
    },
    menuSlider: {
        height: Dimensions.get('window').height / 2 - 30,
        marginTop: 20
    },
    menuItem: {
        backgroundColor: color.WhiteColor,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: color.BlackColor,
        shadowOpacity: .2,
        shadowOffset: { width: 3, height: 3 },
        borderRadius: 10,
        elevation: 2,
        paddingVertical: 10,
        padding: 10,
        height: 'auto',
        margin: 5
    },
    menuItemContainer: {
        flexDirection: 'row',
        marginVertical: 15,
        marginHorizontal: 10,
        justifyContent: 'flex-start',
    },
    menuItemImage: {
        width: 40,
        height: 40,
        resizeMode: 'contain',
        margin: 10
    },
    menuItemLabel: {
        textAlign: 'center'
    },
    linearGradient: {
        width: 100,
        height: 100,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    centerCircle: {
        backgroundColor: colorsStyles.white,
        width: 80,
        height: 80,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    centerCircleText: {
        fontSize: 12
    }, container: {
        flex: 1,
        padding: 20,
    },
    logoContainer: {
        alignItems: "center",
        flexGrow: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        width: 200,
        height: 150
    },
    input: {
        height: 40,
        width: 350,
        marginBottom: 10,
        backgroundColor: "rgba(255,255,255,0.2)",
        color: "#00000",
        paddingHorizontal: 10
    },
    button: {
        height: 50,
        backgroundColor: "rgba(255,255,255,0.2)",
        alignSelf: "stretch",
        marginTop: 10,
        justifyContent: "center",
        paddingVertical: 15,
        marginBottom: 10
    },
    buttonText: {
        fontSize: 18,
        alignSelf: "center",
        textAlign: "center",
        color: "#FFF",
        fontWeight: "700"
    },
    subtext: {
        color: "#ffffff",
        width: 160,
        textAlign: "center",
        fontSize: 35,
        fontWeight: "bold",
        marginTop: 20
    }, SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        margin: 10,
    },
    buttonStyle: {
        backgroundColor: '#000099',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#7DE24E',
        height: 40,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    inputStyle: {
        flex: 1,
        color: 'black',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderColor: '#dadae8',
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    successTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
    },
};
