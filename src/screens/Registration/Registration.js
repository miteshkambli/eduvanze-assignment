import React from 'react';
import {
  TextInput,
  View,
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView
} from 'react-native';
// pull in the ScreenName component from ScreenName.js
import DropDownPicker from 'react-native-dropdown-picker';
import styles from './styles';
import { postRegister } from '../../actions';
import { connect } from 'react-redux';
// pull in header with DrawerTrigger
import Header from '../../components/Header.js'

class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: '',
      dob: '',
      proffession: '',
      guest: '',
      address: '',
    };
  }
  validates = () => {
    const { submit} = this.props;
    let name = this.state.name;
    let age = this.state.age;
    let dob = this.state.dob;
    let proffession = this.state.proffession;
    let guest = this.state.guest;
    let address = this.state.address;
    const data = {
      name,
      age,
      dob,
      proffession,
      guest,
      address
    }
    if (
      name.length === 0 &&
      age.length === 0 &&
      dob.length === 0 &&
      proffession.length === 0 &&
      guest.length === 0 &&
      address.length === 0
    ) {
      alert('All Fields are mandatory');

    } else {
      console.log(data)
      this.props.postRegister(data);
    }
  };
  // we won't need to configure navigationOptions just yet
  static navigationOptions = {

  };

  render() {
    const { data, loading } = this.props.Register;

    return (
      <React.Fragment>
        <Header />
        <View style={styles.container}>
          <ScrollView
            keyboardShouldPersistTaps="handled"
            contentContainerStyle={{
              justifyContent: 'center',
              alignContent: 'center',
            }}>

            <View style={{ alignItems: 'center' }}>
              <Text
                style={{
                  fontSize: 30,
                  fontWeight: 'bold',
                  justifyContent: 'center',
                  alignSelf: 'center',
                  length: 30,
                  alignItems: 'center',
                  marginTop: 10,
                }}>
                Registration
            </Text>
            </View>
            <KeyboardAvoidingView enabled>
            <View style={styles.SectionStyle}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(name) => this.setState({name})}
                underlineColorAndroid="#f000"
                placeholder="Enter Name"
                maxLength={30}
                placeholderTextColor="#000000"
                autoCapitalize="sentences"
                returnKeyType="next"
                blurOnSubmit={false}
              />
            </View>
            <View style={styles.SectionStyle}>
              <TextInput
                style={styles.inputStyle}
                underlineColorAndroid="#f000"
                placeholder="Enter Age"
                maxLength={2}
                onChangeText={(age) => this.setState({age})}
                placeholderTextColor="#000000"
                keyboardType="phone-pad"
                returnKeyType="next"
                blurOnSubmit={false}
              />
            </View>
            <View style={styles.SectionStyle}>
              <TextInput
                style={styles.inputStyle}
                underlineColorAndroid="#f000"
                placeholder="Enter DOB"
                maxLength={8}
                onChangeText={(dob) => this.setState({dob})}
                placeholderTextColor="#000000"
                keyboardType="phone-pad"
                returnKeyType="next"
                blurOnSubmit={false}
              />
            </View>
            <View style={styles.SectionStyle}>
              <DropDownPicker
                items={[
                  {
                    label: 'Student',
                    value: 'Student',
                  },
                  {
                    label: 'Employee',
                    value: 'Employee',
                  },
                ]}
                containerStyle={{height: 40, width: '100%'}}
                style={{backgroundColor: '#fafafa'}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                placeholder="Select an Profession"
                dropDownStyle={{backgroundColor: '#fafafa'}}
                onChangeItem={(item) =>
                  this.setState({
                    proffession: item.value,
                  })
                }
              />
            </View>
            <View style={styles.SectionStyle}>
              <DropDownPicker
                items={[
                  {
                    label: '0',
                    value: '0',
                  },
                  {
                    label: '1',
                    value: '1',
                  },
                  {
                    label: '2',
                    value: '2',
                  },
                ]}
                containerStyle={{height: 40, width: '100%'}}
                style={{backgroundColor: '#fafafa'}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                placeholder="Select an Guest"
                dropDownStyle={{backgroundColor: '#fafafa'}}
                onChangeItem={(item) =>
                  this.setState({
                    guest: item.value,
                  })
                }
              />
            </View>

            <View style={styles.SectionStyle}>
              <TextInput
                style={styles.inputStyle}
                underlineColorAndroid="#f000"
                placeholder="Address"
                onChangeText={(address) => this.setState({address})}
                placeholderTextColor="#000000"
                keyboardType="default"
                maxLength={100}
                returnKeyType="done"
                blurOnSubmit={false}
              />
            </View>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={this.validates}>
              <Text style={styles.buttonTextStyle}>REGISTER</Text>
            </TouchableOpacity>
            </KeyboardAvoidingView>
          </ScrollView>
        </View>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  Register: state.Register,
});

export default connect(mapStateToProps, { postRegister })(
  Registration,
);
